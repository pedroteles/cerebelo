package com.pedroteles.cerebelo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class JogosFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private ListView lstJogos;

    private String[] listaNomeJogo = {"Jogo 1", "Jogo 2", "Jogo 3", "Jogo 4"};
    private String[] listaNomeCategoria = {"Categoria 1", "Categoria 2", "Categoria 3", "Jogo 4"};

    public JogosFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jogos, container, false);

        lstJogos = (ListView) view.findViewById(R.id.lstJogos);

        lstJogos.setAdapter(new CustomAdapterJogos((HomeActivity) getActivity(), listaNomeJogo, listaNomeCategoria));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
