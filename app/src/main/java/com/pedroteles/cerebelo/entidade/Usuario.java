package com.pedroteles.cerebelo.entidade;

/**
 * Created by Pedro Teles on 30/07/2017.
 */

public class Usuario {
    //Atributos
    private String NomeUsuario;
    private String SenhaUsuario;
    private String EmailUsuario;

    //Construtor
    public Usuario() {
        this.NomeUsuario = "";
        this.SenhaUsuario = "";
        this.EmailUsuario = "";
    }

    //Getters e Setters
    public String getNomeUsuario() {
        return NomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        NomeUsuario = nomeUsuario;
    }

    public String getSenhaUsuario() {
        return SenhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        SenhaUsuario = senhaUsuario;
    }

    public String getEmailUsuario() {
        return EmailUsuario;
    }

    public void setEmailUsuario(String emailUsuario) {
        EmailUsuario = emailUsuario;
    }

    public boolean cadastrarUsuario() {
        return false;
    }

    //Métodos
    public Usuario loginUsuario(String nomeUsuario, String senhaUsuario) {
        Usuario user = new Usuario();
        user.setNomeUsuario("BATATA");
        return user;
    }
}
