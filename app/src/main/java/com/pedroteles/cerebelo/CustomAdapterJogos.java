package com.pedroteles.cerebelo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

class CustomAdapterJogos extends BaseAdapter {
    private String[] listaNomeJogo;
    private String[] listaCategoriaJogo;

    private Context context;

    private static LayoutInflater inflater = null;

    CustomAdapterJogos(HomeActivity homeActivity, String[] listaNomeJogo, String[] listaCategoriaJogo) {
        this.listaNomeJogo = listaNomeJogo;
        this.listaCategoriaJogo = listaCategoriaJogo;
        this.context = homeActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaNomeJogo.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class Holder {
        TextView txtTitulo;
        TextView txtCategoria;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.lista_jogos, null);

        holder.txtTitulo = (TextView) rowView.findViewById(R.id.txtTitulo);
        holder.txtCategoria = (TextView) rowView.findViewById(R.id.txtCategoria);

        holder.txtTitulo.setText(listaNomeJogo[position]);
        holder.txtCategoria.setText(listaCategoriaJogo[position]);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You Clicked " + listaNomeJogo[position], Toast.LENGTH_LONG).show();
            }
        });

        return rowView;
    }
}
