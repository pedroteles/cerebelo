package com.pedroteles.cerebelo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pedroteles.cerebelo.entidade.Usuario;

public class CadastroActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout lnlCadastro;

    private EditText edtUsuario;
    private EditText edtEmail;
    private EditText edtSenha;
    private EditText edtConfirmarSenha;

    private Button btnCadastrar;

    private TextView txtLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        //FindViewById
        lnlCadastro = (LinearLayout) findViewById(R.id.lnlCadastro);
        edtUsuario = (EditText) findViewById(R.id.edtUsuario);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        edtConfirmarSenha = (EditText) findViewById(R.id.edtConfirmarSenha);
        btnCadastrar = (Button) findViewById(R.id.btnCadastrar);
        txtLogin = (TextView) findViewById(R.id.txtLogin);

        //SetOnClickListeaners
        lnlCadastro.setOnClickListener(this);
        btnCadastrar.setOnClickListener(this);
        txtLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == lnlCadastro) {
            //MÉTODO PARA FECHAR O TECLADO AO APERTAR FORA
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } else if(view == btnCadastrar) {
            //CRIAÇÃO DA CLASSE USUÁRIO
            Usuario usuario = new Usuario();

            //POPULAR CLASSE USUÁRIO
            usuario.setNomeUsuario(edtUsuario.getText().toString());
            usuario.setSenhaUsuario(edtSenha.getText().toString());
            usuario.setEmailUsuario(edtEmail.getText().toString());

            //CHECA SE TODOS OS CAMPOS FORAM DIGITADOS, SE O EMAIL E SENHA SÃO VÁLIDOS E SE O USUÁRIO EXISTE
            if(caixasVazias()) {
                Toast.makeText(this, getResources().getString(R.string.digite_todos_campos), Toast.LENGTH_SHORT).show();
            } else if(!emailValido(usuario)) {
                Toast.makeText(this, getResources().getString(R.string.email_invalido), Toast.LENGTH_SHORT).show();
            } else if(!confirmarSenha(usuario)) {
                Toast.makeText(this, getResources().getString(R.string.senhas_nao_conferem), Toast.LENGTH_SHORT).show();
            } else if(checarUsuarioExistente(usuario)) {
                Toast.makeText(this, getResources().getString(R.string.usuario_ja_cadastrado), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getResources().getString(R.string.usuario_cadastrado), Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        } else if(view == txtLogin) {
            //MÉTODO PARA VOLTAR A TELA ANTIGA
            onBackPressed();
        }
    }

    //MÉTODO PARA CHECAR SE OS CAMPOS FORAM DIGITADOS
    public boolean caixasVazias() {
        return edtUsuario.getText().toString().equals("") || edtEmail.getText().toString().equals("") || edtSenha
                .getText().toString().equals("") || edtConfirmarSenha.getText().toString().equals("");

    }

    //MÉTODO PARA CHECAR SE O EMAIL DO USUÁRIO É VÁLIDO
    public boolean emailValido(Usuario user) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(user.getEmailUsuario()).matches();
    }

    //MÉTODO PARA CONFIRMAR SE AS SENHAS DIGITADAS CONFEREM
    public boolean confirmarSenha(Usuario user) {
        return user.getSenhaUsuario().equals(edtConfirmarSenha.getText().toString());
    }

    //MÉTODO PARA CHECAR SE JÁ EXISTE UM USUÁRIO COM ESTE NOME
    public boolean checarUsuarioExistente(Usuario user) {
        return false; //TODO: implementar método de checar usuário
    }
}
