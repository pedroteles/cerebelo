package com.pedroteles.cerebelo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pedroteles.cerebelo.entidade.Usuario;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private LinearLayout lnlLogin;
    private EditText edtUsuario;
    private EditText edtSenha;
    private Button btnLogin;
    private TextView txtCadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //FindViewById
        lnlLogin = (LinearLayout) findViewById(R.id.lnlLogin);
        edtUsuario = (EditText) findViewById(R.id.edtUsuario);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtCadastro = (TextView) findViewById(R.id.txtCadastro);

        //SetOnClickListener
        lnlLogin.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        txtCadastro.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view == lnlLogin) {
            //MÉTODO PARA FECHAR TECLADO AO APERTAR FORA
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } else if(view == btnLogin) { //Checa se o botão login foi pressionado
            if(!edtUsuario.getText().toString().equals("") || !edtSenha.getText().toString().equals("")) { //Checa se os valores foram digitados
                Usuario usuario = new Usuario();
                usuario = usuario.loginUsuario(edtUsuario.getText().toString(), edtSenha.getText().toString()); //Retorna um usuário do BD

                if(!usuario.getNomeUsuario().equals("")) { //Checa se o usuário existe
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                } else {
                    Toast.makeText(this, "Usuário e/ou senha inválidos!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Digite todos os campos!", Toast.LENGTH_SHORT).show();
            }

        } else if(view == txtCadastro) { //Checa se o texto de cadastro foi pressionado
            startActivity(new Intent(LoginActivity.this, CadastroActivity.class));
        }
    }
}
